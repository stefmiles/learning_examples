"""
This module contains the class I was to test, using doctests, as a whole.
It also contains a function outside the class in order to understand name
spaces at the same time and how doctests in docstrings are 'tried'.

>>> print_argument(2)
2 is a int

#>>> my_object.multiply()

    
#>>> print('module docstring')
#module docstring
#>>> my_object.add()
#4

#>>> my_object.add_triple(1)
#3


"""


def print_argument(my_num):
    """Print the argument and type int.

    """
    print(str(my_num) + ' is a int')


class Stefs_Doctest_Practice(object):
    """This class contains the following three methods;
    multiply(), make_string() and make_list() for the purpose of
    determining how docstrings and doctest work when functions, classes and
    methods all exist in a module. 
    
    #>>> my_object.multiply()

    
    #>>> my_object.add()
    #4
    
    #>>> my_object.add_triple(5)
    #15

    """
    if __name__=='__main__':
        print('class in main')
    else:
        print('__name__ of mcm is ', __name__)
    
    def __init__(self, n, number, testmode=False):
        """Instantiate the object by creating the following instance
        attributes.
        n is a float.
        """
        self.n = n
        self.testmode = testmode
        if not self.testmode:
            print('NOT TESTING')
        if self.testmode:
            print('TESTING')
        
        self.multiply()
        self.addition_value = self.add()
        triple = self.add_triple(number)

    def multiply(self):
        """Return 3 times the value of n and set the result as an instance
        attribute.
        #>>> my_object.multiply()

        """
        self.multiplied_n = self.n*3
    
    def add(self):
        """return n plus n.

        >>> my_object = Stefs_Doctest_Practice(2, 3)
        NOT TESTING
        >>> add()
        4
        """
        addition_value = self.n + self.n
        return addition_value
    
    def add_triple(self, number):
        """ return triple of that number.
        #>>> my_object.add_triple(8)
        #24
        """
        triple = number + number + number
        return triple




if __name__ =='__main__':
    
    print('IM FROM HERE')
    import doctest
#    my_object = Stefs_Doctest_Practice(2, 3)
    doctest.testmod()

 

