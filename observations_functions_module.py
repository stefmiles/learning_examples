"""
This module contains the class ObMethods, containing similar and some of the
methods from the OWS_Obs class so I can understand how unit testing works.
Here, the 'unit' im testing is the ObMethods class itself.

"""

from datetime import datetime
from datetime import timedelta

class ObMethods():
    
    print('Were in the class  now')
    
    def __init__(self, location):
        self.loc = location
        self.create_blank_observations_dictionary()


    def create_blank_observations_dictionary(self):
        """Create a blank dictionary for observations data with all
        different time_frequency possibilities as its keys.
        """
        observations_dictionary = {}
        observations_dictionary[self.loc] = {} 
        self.ob_dict = observations_dictionary
        #observations_dictionary[loc]['raw_data'] = {}
        #observations_dictionary[loc]['ten_minute_data'] = {}
        #observations_dictionary[loc]['hourly_data'] = {}
        #observations_dictionary[loc]['three_hourly_data'] = {}
        #return observations_dictionary


    def closest_twelve_hour(self, a_date):
        """Calulate the closest twelfth hour datetime to the passed in date
        argument.
        """
        a_date = datetime.strptime(a_date, '%Y%m%d%H%M')
        differences_list = []
        twelfth_hours = [0,12,24]
        for each_hour in twelfth_hours:
            the_diff = (a_date.hour) - each_hour 
            differences_list.append(abs(the_diff))
        closest_index = differences_list.index(min(differences_list))
        closest_hour = twelfth_hours[closest_index]
        if closest_hour == 24:
            closest_datetime = datetime(a_date.year, a_date.month,
                    a_date.day, closest_hour-12)
        else:
            closest_datetime = datetime(a_date.year, a_date.month,
                    a_date.day, closest_hour)
        
        #closest_datetime = closest_datetime.strftime('%Y%m%d%H')    
        return closest_datetime


    def plus_one_day(self, a_date):
        the_day = datetime.strptime(a_date, '%Y%m%d%H')
        next_day = the_day + timedelta(days = 1)
        next_day_str = next_day.strftime('%Y%m%d%H')
        return next_day_str


    def fun(self, x):
        return x+1

# this is an after bitbucket comment

