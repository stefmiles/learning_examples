"""
This module contains the class to test methods of the class ObMethods from
observations_function_module.py

This module creates a unit test with 4 test cases:
test_is_three_plus_one_four
test_one_day_after_is_the_third
test_closest_datetime_is_twelve_pm
test_dictionary_has_Satun
Any member function whose name begins with test in a class deriving from
unittest.TestCase will be run and assertions checked when unittest.main() is
called.

Because tests can be numerous and set up can be repetitive, a set-up code
can be useful by implementing the methods setUp().  Similarly, we can
implement a tearDown() method that tidies up after the test method has been
run.  This environment for testing code is called a *fixture*.







"""


from datetime import datetime
import unittest
import datetime
import observations_functions_module as ofm

from observations_functions_module import ObMethods


class ObsFunctTestCase(unittest.TestCase):
    """ Tests for observations_functions_module.py"""
    
    def setUp(self):
        self.my_ob = ObMethods('Satun')


    def test_is_three_plus_one_four(self):
        """ If 3+1 equal to four?"""
        self.assertTrue(self.my_ob.fun(3),4)

    def test_one_day_after_is_the_third(self):
        """ Is one day after Jan 2nd, Jan 3rd?"""
        self.assertTrue(self.my_ob.plus_one_day('2017010200'), '2017010300')
    
    def test_closest_datetime_is_twelve_pm(self):
        """Is the closest datetime successfully determined as being
        Jan 13 12:00 2016? """
        self.assertEqual(type(self.my_ob.closest_twelve_hour('2017011335')), datetime.datetime)
        self.assertTrue(type(self.my_ob.closest_twelve_hour('201701131300'))==datetime.datetime)
        self.assertEqual((self.my_ob.closest_twelve_hour('201701131300')).day, 13)
        self.assertEqual((self.my_ob.closest_twelve_hour('201701131300')).hour,12)
    
    def test_dictionary_has_Satun(self):
        #self.assertEqual(self.my_ob.ob_dict, {'Satun':{}})
        #self.assertTrue(type(self.my_ob.ob_dict), dict) 
        #self.assertEqual((self.my_ob.ob_dict).keys(),['Satun'])
        #self.assertEqual(len((self.my_ob.ob_dict).keys()), 1)   
        self.assertIn('Satun', self.my_ob.ob_dict)

    def tearDown(self):
        self.my_ob = None
       

if __name__ == '__main__':
    
    #ob_object = ObMethods('Satun')
    unittest.main()



#For obs module, want to test
# blank obs dict 
# blank stats dict
# expected datetime list
# get obs from file NEED THIS ONE and test all locations for diff type of verification
# mean ten minute 
# closest value
# make different time frequencies lists NEED THESE ONES
# create variable array

