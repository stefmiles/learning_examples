"""
This module is to help me learn what are and how to use, doctest's and will
be used as an example.

The module supplies the function multiply(), which I wrote.  For example, 

>>> multiply(5)
15


It also includes the functions multiply_and_add() and unpredictable_date()
which I also wrote.

Running the module as a script (either in  the command line via typing in
'' python doctest_example.py '' or opening ipython and typing 
'' run doctest_example.py '') will display nothing unless something fails.  
Running the module as a script with '' -v '' will give a detailed report of
all examples tried. 

Running the module examines all module and function docstrings and will
check both examples written in these docstrings. 
These docstring examples are recognised by the interpreter prompt '>>>' or
'...'. These example cases must be ended by a blank line, or by the next
interpreter prompt.  

The unpredictable_date function is included to illustrate that there are
cases where the exact output may not be predictable, but should still be
testable.  Usually, object identifiers are based on the memory address of
the data structure holding the object so everytime the script is run, these
id values change.  Using '' ELLIPIS '' tells doctest to ignore portions of
the verification value where '...' is written.

If this module was imported into and used via another script (for example 
Ill call it stefs_new_module.py) everything in this modules namespace is
imported into stefs_new_module's namespace - the main namespace.  Therefore,
the namespace for this module, doctest_example, is not in main anymore and 
doctest is not imported


If run doctest_example.py in ipython, 
1. my_number = multiply_and_add(54) on line 74 is executed
2. my_number is printed
3. doctest module is imported
4. 'doctest is now imported' is printed
5. doctests are examined.

"""

from datetime import datetime
from datetime import timedelta


def multiply(n):
    """Return 3 times the value of n."""
    return n*3


def multiply_and_add(n):
    """Return 3 times the value of n, plus 1.
    >>> multiply_and_add(3)
    10

    """
    n = (n*3) +1
    return n


def unpredictable_date(a_date):
    """Return a datetime five hours after the passed in date argument.
    >>> unpredictable_date(datetime.now()) # doctest: +ELLIPSIS
    datetime.datetime(...)

    """
    hours_to_add = timedelta(hours=5)
    future_date = a_date + hours_to_add
    return future_date


my_number = multiply_and_add(54)
print(my_number)

if __name__ == "__main__":
    import doctest
    print('doctest is now imported')
    doctest.testmod()

