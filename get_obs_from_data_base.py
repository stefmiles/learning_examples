import csv
import owsdb_ssh
from collections import defaultdict
import location_information as li 
import os
from datetime import datetime 
import numpy as np
import sys

def create_and_fill_blank_observations_dictionary(loc):
    observations_dictionary = create_blank_observations_dictionary(loc)
    continue_nxt_loc = get_observations_data(loc, obs_file_name_list, obsbasepath)
    #if continue_nxt_loc
    return continue_nxt_loc

def observations_file_path(loc, obs_file_name):
    if loc == 'Ichthys':
        file_path = 'Y:/MOBS/ICHTHYS/' + 'ichthys_' + obs_file_name + '.txt'
    else:
        file_path = obsbasepath + loc + '/' + obs_file_name +'.csv'
    return file_path


def get_observations_data(loc, obs_file_name_list, obsbasepath):
    for obs_file_name in obs_file_name_list: 
        obs_file_path = observations_file_path(loc, obs_file_name)                    
        if os.path.isfile(obs_file_path):
            get_data_from_csv(obs_file_path)
        else:
            if loc == 'Cuulong' or loc == 'PortKembla' or loc == 'Ichthys' or loc == 'Champion':
                print('this location doesnt come from the database... try elsewhere please')
                obs_data_exists = False
                continue
            else:
                the_month = obs_file_name[4:]
                the_year = obs_file_name[0:4]
                # download the data
                print('downloading data...')
                obs_data_exists = download_from_database(loc, obs_file_path, the_month, the_year)               
            if obs_data_exists:
                get_data_from_csv(obs_file_path)




def get_data_value(line, column,loc):
    
    if loc == 'Ichthys':
        line_split =line.split()
    else: 
        line_split =line.split(',')
    val= np.nan
    try:
        val = line_split[column]
    except:
        val = np.nan
    return val




def get_data_from_csv(obs_file_path):
    tempo_dict={}
    tempo_dict[loc]={}
    tempo_dict[loc]['raw_data']={}
    start_date = datetime(2016,3,1,0,0)
    print(obs_file_path)
    with open(obs_file_path,'r') as fop:
        line_count = True
        for line in fop:
            if line_count:
                if loc == 'Ichthys':
                    headings_list = line.split()
                    headings_list[-1] = headings_list[-1].strip()
                    date1_index = 0
                    date2_index = 1
                    date_format = '%Y%m%d%H%M'
                else:
                    headings_list = line.split(',')
                    for i in range(len(headings_list)):
                        headings_list[i] = headings_list[i].strip()
                    date1_index, date2_index = variable_index_from_heading('date', headings_list)
                    date_format = get_datetime_format(date1_index, loc)
                hs1_index, hs2_index = variable_index_from_heading('hs_variable', headings_list)
                wspd1_index, wspd2_index = variable_index_from_heading('wspd_variable', headings_list)
                wdir1_index, wdir2_index = variable_index_from_heading('wdir_variable', headings_list)
                temp1_index, temp2_index = variable_index_from_heading('temp_variable', headings_list)
                rain1_index, rain2_index = variable_index_from_heading('rain_variable', headings_list)
                opt1_index, opt2_index = variable_index_from_heading('optsen', headings_list)
                
                line_count = False
                continue
            if loc == 'Ichthys':
                the_date = datetime.strptime((line.split()[date1_index] + line.split()[date2_index]),date_format)
            else:
                the_date = datetime.strptime(get_data_value(line, date1_index, loc),date_format)
            #print(the_date)
            #print(get_data_value(line, hs1_index, loc).strip('"'))
            if the_date == start_date:
                optsen_num = int(float(get_data_value(line, opt1_index, loc).strip()))
                tempo_dict[loc]['raw_data'][the_date] = {}
                if hs1_index >= 0:
                    try:
                        tempo_dict[loc]['raw_data'][the_date]['hs_variable'] = (round(float(get_data_value(line, hs1_index, loc).strip('"')),2))
                    except:
                        tempo_dict[loc]['raw_data'][the_date]['hs_variable'] = np.nan
                if wspd1_index >= 0 or wspd2_index >= 0:
                    try:
                        if opt1_index >0:
                            if optsen_num ==1:
                                tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = (round(float(get_data_value(line, wspd1_index, loc).strip('"')),2))
                            else:
                                tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = (round(float(get_data_value(line, wspd2_index, loc).strip('"')),2))
                        else:
                            tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = np.nan
                    except:
                        tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = np.nan
                if wdir1_index >= 0 or wdir2_index >=0:
                    try:
                        if opt1_index >0:
                            if optsen_num ==1:
                                tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = (round(float(get_data_value(line, wdir1_index, loc).strip('"')),2))
                            else:
                                tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = (round(float(get_data_value(line, wdir2_index, loc).strip('"')),2))
                        else:
                            tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = np.nan
                    except:
                        tempo_dict[loc]['raw_data'][the_date]['wspd_variable'] = np.nan
                if temp1_index >= 0:
                    try:
                        tempo_dict[loc]['raw_data'][the_date]['temp_variable'] = (round(float(get_data_value(line, temp1_index, loc).strip('"')),2))
                    except:
                        tempo_dict[loc]['raw_data'][the_date]['hs_variable'] = np.nan
                if rain1_index >= 0:
                    try:
                        tempo_dict[loc]['raw_data'][the_date]['rain_variable'] = (round(float(get_data_value(line, rain1_index, loc).strip('"')),2))
                    except:
                        tempo_dict[loc]['raw_data'][the_date]['rain_variable'] = np.nan




            #else:
            #    print('date_not in')
        print(tempo_dict)
        print('exiting...')
        sys.exit()




def get_datetime_format(date_index, loc):
    if date_index == 1:
        date_format = '"%Y-%m-%d %H:%M:%S"'
    elif date_index == 0:
        if loc == 'Champion' or loc == 'Cuulong':
            date_format = '%d/%m/%Y %H:%M'
        elif loc == 'PortKembla':
            date_format = '%m/%d/%Y %H:%M'
        else:
            date_format = '%Y-%m-%d %H:%M:%S+00:00'
    return date_format

def variable_index_from_heading(variable, headings_list): 
    if variable == 'optsen':
        for opt_naming in ['"opt_wind_sensor"', 'optsen']:
            first_index = -1
            second_index = -1
            try:
                first_index = headings_list.index(opt_naming)
                break
            except:
                continue
        print('op1', first_index)
        print('op2', second_index)


    if variable == 'date':
        for date_naming in ['"utc_time"', 'date', 'datetime', 'Date & Local Time', 'Date/Time']:
            first_index = -1
            second_index = -1
            try:

                first_index = headings_list.index(date_naming)
                
                print(first_index)
                break
            except:
                continue
        print('date1', first_index)
        print('date2', second_index)

    if variable == 'hs_variable':
        for hs_naming in ['Hsig','Hs (m)', '"significant_wave_height"', '"total_significant_wave_height"', '"sig_wave"', '"total_significant_wave_height_m_wd1"', 'hs','Hsig (m)']:
            first_index = -1                             
            second_index = -1
            try:
                first_index = headings_list.index(hs_naming)
                break
            except:
                continue
        print('hs1', first_index)
        print('hs2', second_index)

    if variable == 'temp_variable':
        for temp_naming in ['"temperature"', "air_temperature_degrees_celcius_10_minute_tr1", 'temp']:
            first_index = -1
            second_index = -1
            try:
                first_index = headings_list.index(temp_naming)
                break
            except:
                continue
        print('t1', first_index)
        print('t2', second_index)

    if variable == 'rain_variable':
        for rain_naming in ['"rain"', 'rain']:
            first_index = -1
            second_index = -1
            try:
                first_index = headings_list.index(rain_naming)
                break
            except:
                continue
            print('rain1', first_index)
            print('rain2', second_index)

        print('rain1', first_index)
        print('rain2', second_index)
    if variable == 'wspd_variable':
        for wspd_naming in ['"wind_speed"','"wind_speed_knots_10_min_an1"', 'wspd1']:
            first_index = -1
            second_index = -1
            try:
                first_index = headings_list.index(wspd_naming)
                if first_index > 0:
                    for wspd2_naming in ['"wind_speed_1minute"','"wind_speed_knots_10_min_an2"', 'wspd2']:
                        try:
                            second_index = headings_list.index(wspd2_naming)
                            break
                        except:
                            continue 
                    break
            except:
                continue
        print('ws1', first_index)
        print('ws2', second_index)

    if variable == 'wdir_variable':
        for wdir_naming in ['"wind_direction"','"wind_direction_degreesfrom_an1"', 'wdir1']:
            first_index = -1
            second_index = -1
            try:
                first_index = headings_list.index(wdir_naming)
                if first_index > 0:
                    for wdir2_naming in ['"wind_direction_1minute"','"wind_direction_degreesfrom_an2"', 'wdir2']:
                        try:
                            second_index = headings_list.index(wdir2_naming)
                            break
                        except:
                            continue
                    break
            except:
                continue
        print('wd1',     first_index)
        print('wd2', second_index)
    return first_index, second_index



            
    


def download_from_database(loc, obs_file_path, the_month, the_year):
    var_source_list, var_name_list = li.database_variables(loc, vtype)
    if not len(var_source_list):
        return False
    else:
        csv_file = obs_file_path
        year = the_year
        month = the_month
        rows = defaultdict(dict)
        with owsdb_ssh.Connection() as conn:
            cur = conn.cursor() 
            for var in var_source_list:
                cur.execute("SET TimeZone = 'UTC'; "
                            "SELECT obs_time, obs_quantity "
                            "FROM weather.wx_obs_data "
                            "WHERE variable_id=%s "
                            "AND source_id=%s "
                            "AND date_part('year', obs_time)=%s "
                            "AND date_part('month', obs_time)=%s; ",
                            (var[0], var[1], year, month))
                results = cur.fetchall()
                for result in results:
                    rows[result[0]]['datetime'] = result[0]
                    rows[result[0]][var[2]] = result[1]
        
        datetimeList = rows.keys()
        datetimeList.sort()
        with open(csv_file, 'wb') as csvfile:
            fieldnames = var_name_list
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for dt in datetimeList:
                writer.writerow(rows[dt])
            return True
        print('{} written!'.format(csv_file))


def create_blank_observations_dictionary(loc):
    observations_dictionary = {}
    observations_dictionary[loc] = {} 
    observations_dictionary[loc]['raw_data'] = {}
    observations_dictionary[loc]['ten_minute_data'] = {}
    observations_dictionary[loc]['hourly_data'] = {}
    observations_dictionary[loc]['three_hourly_data'] = {}
    return observations_dictionary












locations = ['Benchamas']
obs_file_name_list = ['201603']
obsbasepath ='Z:/MVN/all_verification_types/WRF/' 

vtype = 'WRF'

for loc in locations:
    continue_nxt_loc = create_and_fill_blank_observations_dictionary(loc)



"""
def get_observations_data_from_file(self, obs_file_path, loc):
        try:
            date_column, hs_column, wspd_column, wdir_column, temp_column, rain_column, data_freq = li.column_number(loc, self._type_of_verification)
        except:
            if not self.in_test:
                print('This location isnt correct. Please check your spelling'
                        'or the locations list, and try again!')
            exit()
        
        
        
        
        
        if not os.path.isfile(obs_file_path):
            if not self.in_test:
                #print('Obs file doesnt exist!', obs_file_path)
                



            #self.get_observations_data_when_file_fails(loc)
        else:
            with open(obs_file_path,'r') as fop:
                line_count = True
                for line in fop:
                    if line_count:
                        line_count = False
                        continue
                    if self._type_of_verification == 'WRF' or self._type_of_verification == 'Global_Winds':
                        hs_data = np.nan
                        temp_data = self.get_data_value(line, temp_column)
                        rain_data = self.get_data_value(line, rain_column)
                        if loc == 'Champion' or loc == 'Cuulong': 
                            the_date = datetime.strptime(self.get_data_value(line, date_column), '%d/%m/%Y %H:%M')
                            wspd_data = self.get_data_value(line, wspd_column)
                            wdir_data = self.get_data_value(line, wdir_column) 
                        elif loc == 'PortKembla': 
                            the_date = datetime.strptime(self.get_data_value(line, date_column), '%m/%d/%Y %H:%M')
                            wspd_data = self.get_data_value(line, wspd_column)
                            wdir_data = self.get_data_value(line, wdir_column)
                        else:
                            the_date = datetime.strptime(self.get_data_value(line, date_column),'"%Y-%m-%d %H:%M:%S"')
                            if obs_file_path.count('fso') or loc == 'Arthit' or loc == 'RPS_North_Rankin' or loc == 'Woodside_North_Rankin' or loc == 'Woodside_Mermaid_Sound_LPG':    
                                wspd_data = self.get_data_value(line, wspd_column)
                                wdir_data = self.get_data_value(line, wdir_column)
                            else:
                                line_split = line.split(',') 
                                optical_wind_sensor = int(line_split[12].replace('\"',''))
                                if optical_wind_sensor == 1:
                                    wspd_data = self.get_data_value(line, wspd_column)
                                    wdir_data = self.get_data_value(line, wdir_column)
                                else:
                                    wspd_data = self.get_data_value(line, (wspd_column+3))
                                    wdir_data = self.get_data_value(line, (wdir_column+3))
                    else:     
                        if loc == 'Ichthys':
                            line_split = line.split()
                            the_date = datetime.strptime(line_split[0]+line_split[1], '%Y%m%d%H%M')
                            try:
                                hs_data = line_split[hs_column]
                            except:
                                hs_data = np.nan
                        elif loc == 'Arthit':
                            the_date = datetime.strptime(self.get_data_value(line, date_column), '"%Y-%m-%d %H:%M:%S"') 
                            hs_data = self.get_data_value(line, hs_column)
                            if float(hs_data.strip('"')) == 0.0:
                                hs_data = np.nan
                        else:
                            if loc == 'PortKembla':
                                the_date = datetime.strptime(self.get_data_value(line, date_column), '%m/%d/%Y %H:%M')
                            elif loc=='Champion':
                                the_date = datetime.strptime(self.get_data_value(line, date_column), '%d/%m/%Y %H:%M')
                            elif loc == 'Cuulong':
                                the_date = datetime.strptime(self.get_data_value(line, date_column), '%d/%m/%Y %H:%M')
                            else:
                                the_date = datetime.strptime(self.get_data_value(line, date_column), '"%Y-%m-%d %H:%M:%S"') 
                            hs_data = self.get_data_value(line, hs_column)
                        wspd_data = np.nan
                        wdir_data = np.nan
                        temp_data = np.nan    
                        rain_data = np.nan 
                    
                    if the_date >= self.start_date and the_date <= self.end_date: 
                        self.observations_dictionary[loc]['raw_data'][the_date] = {}
                        try:
                            self.observations_dictionary[loc]['raw_data'][the_date]['hs_variable'] = round(float(hs_data.strip('"')),2)
                        except:
                            self.observations_dictionary[loc]['raw_data'][the_date]['hs_variable'] = np.nan
                        try:
                            self.observations_dictionary[loc]['raw_data'][the_date]['wspd_variable'] = round(float(wspd_data.strip('"')),2)
                        except:
                            self.observations_dictionary[loc]['raw_data'][the_date]['wspd_variable'] = np.nan
                        try:
                            self.observations_dictionary[loc]['raw_data'][the_date]['wdir_variable'] = round(float(wdir_data.strip('"')),2)
                        except:
                            self.observations_dictionary[loc]['raw_data'][the_date]['wdir_variable'] = np.nan
                        try:
                            self.observations_dictionary[loc]['raw_data'][the_date]['rain_variable'] = round(float(rain_data.strip('"')),2)
                        except:
                            self.observations_dictionary[loc]['raw_data'][the_date]['rain_variable'] = np.nan
                        try:
                            self.observations_dictionary[loc]['raw_data'][the_date]['temp_variable'] = round(float(temp_data.strip('"')),2)
                        except:
                            self.observations_dictionary[loc]['raw_data'][the_date]['temp_variable'] = np.nan
                    else:
                        continue
           
"""

