#!/c/users/operator/appdata/local/enthought/canopy/user/scripts/python
import os 



def create_missing_folder(base_path, folder_name, year, testmode = True):
    print('in create_missing_folder')
    
    if not os.path.isdir(base_path+year):
        os_command = 'mkdir ' + base_path + folder_name + '\\' + year
        if testmode:
            print(os_command)
        else:
            print(os_command)
            os.system(os_command)
    for month in ['01','02','03','04','05','06','07','08','09','10','11','12']:
        if not os.path.isdir(base_path+folder_name+'\\'+year+'\\'+year+month):
            #os.mkdir('N:/Models/WW3_output/'+folder_name+'/'+year+'/'+year+month)
            os_command = 'mkdir '+base_path+folder_name+'\\'+year+'\\'+year+month
            if testmode:
                print(os_command)
            else:
                print(os_command)
                os.system(os_command)


            #os.mkdir(os_command)
        for folder in os.listdir(base_path+folder_name+'\\'):
            if year+month in folder:
                os_command = 'mv ' + base_path+folder_name+'\\'+folder +' '+ base_path+folder_name+'\\'+year+'\\'+year+month
                if testmode:
                    print(os_command)
                else:
                    print(os_command)
                    os.system(os_command)
                #shutil.move('N:/Models/WW3_output/'+folder_name+'/'+year+'/'+folder, 'N:/Models/WW3_output/'+folder_name+'/'+year+'/'+year+month)








    







if __name__ == '__main__':
    print('hello')
    base_path = 'N:\\Models\\WW3_output\\' 

    clean_list_globals = ['GFS418_Global','GFS_Global','GFST2_Global','NGM_Global','APS_Global', 'APS1_Global','CMC_Global','GFX_Global']
    clean_list_gfs418 = ['BASSX_GFS418_Nested','BIGHT_GFS418_Nested','CHINA_GFS418_Nested','JAVA_GFS418_Nested','SCS_GFS418_Nested','TIMOR_GFS418_Nested','WA_GFS418_Nested','EC_GFS418_Nested']
    clean_list_gfs = ['BASSX_GFS_Nested','BIGHT_GFS_Nested','CHINA_GFS_Nested','JAVA_GFS_Nested','SCS_GFS_Nested','TIMOR_GFS_Nested','WA_GFS_Nested']
    clean_list_gfx = ['BENGAL_GFX_Nested']
    clean_list_ngm = ['BASSX_NGM_Nested','BENGAL_NGM_Nested','BIGHT_NGM_Nested','CHINA_NGM_Nested','JAVA_NGM_Nested','SCS_NGM_Nested','TIMOR_NGM_Nested','WA_NGM_Nested']
    clean_list_ec = ['BASSX_EC_Nested','BENGAL_EC_Nested','BIGHT_EC_Nested','CHINA_EC_Nested','JAVA_EC_Nested','SCS_EC_Nested','TIMOR_EC_Nested','WA_EC_Nested']
    clean_list_aps = ['BASSX_APS_Nested','BENGAL_APS_Nested','BIGHT_APS_Nested','CHINA_APS_Nested','JAVA_APS_Nested','SCS_APS_Nested','TIMOR_APS_Nested','WA_APS_Nested']
    clean_list_aps1= ['BASSX_APS1_Nested','BENGAL_APS1_Nested','BIGHT_APS1_Nested','CHINA_APS1_Nested','JAVA_APS1_Nested','SCS_APS1_Nested','TIMOR_APS1_Nested','WA_APS1_Nested']
    clean_list_cmc = ['BASSX_CMC_Nested','BENGAL_CMC_Nested','BIGHT_CMC_Nested','CHINA_CMC_Nested','JAVA_CMC_Nested','SCS_CMC_Nested','TIMOR_CMC_Nested','WA_CMC_Nested']
    clean_list_wrf = ['WRF_NWS_WA418_Nested', 'WRF_SCS_GFS418_Nested', 'WRF_THAI_SCS418_Nested','WRF_TIMOR_GFS418_Nested']
    clean_list= clean_list_globals + clean_list_gfs418 + clean_list_gfs + clean_list_ngm + clean_list_ec + clean_list_aps + clean_list_aps1 + clean_list_cmc + clean_list_wrf + clean_list_gfx

    #clean_list  = ['JAVA_GFS418_Nested']
    for domain in clean_list:
        if os.path.isdir(base_path+domain) == False:
            continue
        print 'moving data for '+domain

        #   folder = 'WA_GFS418_Nested'
        year = '2017'

        testmode = True
        testmode = False

        create_missing_folder(base_path, domain, year, testmode=testmode)





def organise_model_data_folders(self):

    clean_list_globals = ['GFS418_Global','GFS_Global','GFST2_Global','NGM_Global','APS_Global', 'APS1_Global','CMC_Global','GFX_Global']
    clean_list_gfs418 = ['BASSX_GFS418_Nested','BIGHT_GFS418_Nested','CHINA_GFS418_Nested','JAVA_GFS418_Nested','SCS_GFS418_Nested','TIMOR_GFS418_Nested','WA_GFS418_Nested','EC_GFS418_Nested']
    clean_list_gfs = ['BASSX_GFS_Nested','BIGHT_GFS_Nested','CHINA_GFS_Nested','JAVA_GFS_Nested','SCS_GFS_Nested','TIMOR_GFS_Nested','WA_GFS_Nested']
    clean_list_gfx = ['BENGAL_GFX_Nested']
    clean_list_ngm = ['BASSX_NGM_Nested','BENGAL_NGM_Nested','BIGHT_NGM_Nested','CHINA_NGM_Nested','JAVA_NGM_Nested','SCS_NGM_Nested','TIMOR_NGM_Nested','WA_NGM_Nested']
    clean_list_ec = ['BASSX_EC_Nested','BENGAL_EC_Nested','BIGHT_EC_Nested','CHINA_EC_Nested','JAVA_EC_Nested','SCS_EC_Nested','TIMOR_EC_Nested','WA_EC_Nested']
    clean_list_aps = ['BASSX_APS_Nested','BENGAL_APS_Nested','BIGHT_APS_Nested','CHINA_APS_Nested','JAVA_APS_Nested','SCS_APS_Nested','TIMOR_APS_Nested','WA_APS_Nested']
    clean_list_aps1= ['BASSX_APS1_Nested','BENGAL_APS1_Nested','BIGHT_APS1_Nested','CHINA_APS1_Nested','JAVA_APS1_Nested','SCS_APS1_Nested','TIMOR_APS1_Nested','WA_APS1_Nested']
    clean_list_cmc = ['BASSX_CMC_Nested','BENGAL_CMC_Nested','BIGHT_CMC_Nested','CHINA_CMC_Nested','JAVA_CMC_Nested','SCS_CMC_Nested','TIMOR_CMC_Nested','WA_CMC_Nested']
    clean_list_wrf = ['WRF_NWS_WA418_Nested', 'WRF_SCS_GFS418_Nested', 'WRF_THAI_SCS418_Nested','WRF_TIMOR_GFS418_Nested']
    clean_list= clean_list_globals + clean_list_gfs418 + clean_list_gfs + clean_list_ngm + clean_list_ec + clean_list_aps + clean_list_aps1 + clean_list_cmc + clean_list_wrf + clean_list_gfx

    year = self.start_date.year
    for folder_name in clean_list:
        if not os.path.isdir(self.modelbasepath + folder_name):
            continue
        if not os.path.isdir(self.modelbasepath+year):
            os_command = 'mkdir ' + self.modelbasepath + folder_name + '\\' + year
            if testmode:
                print(os_command)
            else:
                #print(os_command)
                os.system(os_command)
        for month in ['01', '02', '03', '04', '05', '06', '07', '08', '09',
                '10', '11', '12']
            if not os.path.isdir(self.modelbasepath + folder_name + '\\' + year + '\\' + year + month):
                os_command = 'mkdir ' + self.modelbasepath + folder_name + '\\'+ year + '\\' + year + month
                if testmode:
                    print(os_command)
                else:
                    #print(os_command)
                    os.system(os_command)
            for folder in os.listdir(self.modelbasepath + folder_name + '\\'):
                if year+month in folder:
                    os_command = 'mv ' + self.modelbasepath + folder_name + '\\' + folder + ' ' + self.modelbasepath + folder_name + '\\'+ year + '\\' + year + month
                    if testmode:
                        print(os_command)
                    else:
                        #print(os_command)
                        os.system(os_command)





