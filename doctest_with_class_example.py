"""
This module will be the script that is run.
Notes on importing:
    style 1:
        import my_class_module as mcm
        This means when instantiating 'my_object', the notation needs
        to be as follows:
            my_object = mcm.Stefs_Doctest_Practice(n)
        This will enable the print_argument() function to be available
        as:
            mcm.print_argument()
    style 2:
        from my_class_module import Stefs_Doctest_Practice
        This will import the class ready to use which means when
        instantiating 'my_object', the notation needs to be as follows:
            my_object = Stefs_Doctest_Practice(n)
        This means that print_argument() is not available to be used.
    style 3.
        import my_class_module
        THIS IS RISKY, DONT DO! 

>>> string_plus_number(3)
'3 Stef'

Findings:
    - docstrings in this module, where __name__=='__main__' will be
    examined. 
    - only examples in docstrings in functions and classes reachable from
    module where __name__=='__main__' will be examined.
    - if in ipython, import this module, doctest_with_class_example.py as m
        Then, anything reachable as 'm.' will be examined. 
        In this case, this will ONLY be:
        - doctests in __name__=='__main__' doctrings                (m.__doc__)
        - doctests in string_plus_number() docstrings               (m.string_plus_number.__doc__)
        - doctests in documentation of int(python documentation)    (m.n.__doc__)
        - NOT doctests in docstrings of Stefs_Doctest_Practice class   (m.my_object.__doc__)
        - NOT doctests in docstrings of my_class_module                (m.mcm.__doc__)
"""



def string_plus_number(my_num):
    """Return a string of string + my_num.
    >>> string_plus_number(23)
    '23 Stef'

    """
    string_to_add = ' Stef'
    number_as_string = str(my_num)
    string_combo = number_as_string + string_to_add
    return string_combo 

my_dict = {}
n = 5

import my_class_module as mcm
my_object = mcm.Stefs_Doctest_Practice(n, testmode=True)


#from my_class_module import Stefs_Doctest_Practice
#my_object = Stefs_Doctest_Practice(n)

#import my_class_module
#my_object = my_class_module.Stefs_Doctest_Practice(n)



if __name__ =='__main__':
    print('IM FROM HERE')
    import doctest
    doctest.testmod()

    
